import requests
from urllib.parse import urljoin
import sys
import xml.etree.ElementTree as ET

GREEN = "\033[32m[+]\033[0m"
RED = "\033[31m[-]\033[0m"

def check_wp_json_leak(url):
    wp_json_url = urljoin(url, "/wp-json/wp/v2/users/")
    response = requests.get(wp_json_url)

    if response.status_code == 200:
        users = [(user['id'], user['name']) for user in response.json()]
        print(f"{GREEN} Logins disclosure /wp-json/wp/v2/: {users}")
    else:
        print(f"{RED} /wp-json/wp/v2/ is not available")


def check_author_sitemap(url):
    author_sitemap_url = urljoin(url, "/author-sitemap.xml")
    response = requests.get(author_sitemap_url)

    if response.status_code == 200:
        root = ET.fromstring(response.text)
        ns = {"ns": "http://www.sitemaps.org/schemas/sitemap/0.9"}
        links = [loc.text for loc in root.findall(".//ns:loc", ns)]
        print(f"{GREEN} Logins disclosure /author-sitemap.xml: {links}")
    else:
        print(f"{RED} author-sitemap.xml not found")


def check_directory_listing(url):
    response = requests.get(url + '/uploads/')

    if "Index of /" in response.text:
        print(f"{GREEN} Directory listing enabled: {url}")
    else:
        print(f"{RED} Directory listing disabled")

def check_wp_config(url):
    wp_config_url = urljoin(url, "/wp-config.php")
    response = requests.get(wp_config_url)

    if "DB_NAME" in response.text or "DB_USER" in response.text:
        print(f"{GREEN} wp-config.php: {wp_config_url}")
    else:
        print(f"{RED} wp-config.php not found")

def check_xmlrpc_methods(url):
    xmlrpc_url = urljoin(url, "/xmlrpc.php")
    xml_data = '''
    <methodCall>
        <methodName>system.listMethods</methodName>
        <params></params>
    </methodCall>
    '''

    headers = {'Content-Type': 'text/xml'}
    response = requests.post(xmlrpc_url, data=xml_data, headers=headers)

    if response.status_code == 200:
        root = ET.fromstring(response.text)
        methods = [string.text for string in root.findall(".//string")]
        print(f"{GREEN} List of enabled methods via xmlrpc: {methods}")
    else:
        print(f"{RED} xmlrpc is not available")


def check_login_form(url):
    wp_login_url = urljoin(url, "/wp-login.php")
    response = requests.get(wp_login_url)

    if response.status_code == 200 and "user_login" in response.text and "user_pass" in response.text:
        print(f"{GREEN} Admin interface: {wp_login_url}")
    else:
        print(f"{RED} Admin interface not found")


if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Usage: python3 script.py <URL WordPress>")
        sys.exit(1)

    target_url = sys.argv[1].strip()
    if not target_url.startswith("http"):
        target_url = "http://" + target_url

    check_wp_json_leak(target_url)
    check_author_sitemap(target_url)
    check_directory_listing(target_url)
    check_wp_config(target_url)
    check_xmlrpc_methods(target_url)
    check_login_form(target_url)


# WP_validator


This script is designed to automatically detect typical WordPress misconfigurations.

At the moment it supports detection:
1) revealing logins via /wp-json/v2/
2) disclosure of logins through sitemap.xml
3) directory listing detection
4) enabled XMLRPC.php detection

https://w0lfreak.medium.com/wordpress-recon-96fe123f91fc


https://t.me/pain_test
